#include<stdio.h>

struct student
{
    int a;
    int b;
};

int main(void)
{
    // test 1
    /*printf("*** %ld\n", (long) &(((struct test *)1)->b));*/
    /*printf("*** %ld\n", (long) &(((struct test *)0)->b));*/


    // test 2 
    struct student *pc;
    typeof((pc) + 1) a;
    printf ("*** sizeof(a) is : %d\n", sizeof(a));


    return 0;
}
