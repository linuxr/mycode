#include<stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
    pid_t pid = getpid();
    printf("pid=%d\n", pid);
    pid_t tid = getpid();
    printf("tid=%d\n", tid);

    return 0;
}
