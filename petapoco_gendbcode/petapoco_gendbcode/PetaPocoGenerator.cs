using System;
using System.IO;
using System.Text;
using System.Linq;

namespace petapoco_gendbcode
{
	public class PetaPocoGenerator
	{
		const string NAMESPACE = @"<#=Namespace#>";
		const string REPONAME = @"<#=RepoName#>";
		const string TABLESANDCOLUMN = @"<#=TablesAndColumns#>";
		const string CONNECTIONSTRINGNAME = @"<#=ConnectionStringName#>";

		public PetaPocoGenerator ()
		{
		}

		public void GenerateDB(string ns, string rn, string connName, Tables tables)
		{
			string DBTemplateFilePath = @"DBTemplate.txt";
			string DBFilePath = @"Database.cs";

			FileStream dbTempfile = new FileStream(DBTemplateFilePath, FileMode.Open, FileAccess.Read);
			FileStream dbfile = new FileStream(DBFilePath, FileMode.OpenOrCreate, FileAccess.Write);

			try
			{
				// read template from db template file to steam -> DBTemplate.txt
				StreamReader sr = new StreamReader(dbTempfile);
				string templateContent = sr.ReadToEnd();

				// replace TableAndColumn
				templateContent = templateContent.Replace(TABLESANDCOLUMN, 
				                                          GenerateTablesAndColumn(tables));

				// replace NameSpace
				templateContent = templateContent.Replace(NAMESPACE, ns);

				// replace ConnectionStringName
				templateContent = templateContent.Replace(CONNECTIONSTRINGNAME, connName);

				// replace RepoName
				templateContent = templateContent.Replace(REPONAME, rn);

				// write content to db file -> Database.cs
				StreamWriter sw = new StreamWriter(dbfile);
				sw.Write(templateContent);
				sw.Flush();
			}
			catch(Exception ex)
			{
				Console.WriteLine("===================================");
				Console.WriteLine(ex.Message);
				Console.WriteLine("===================================");
			}
			finally
			{
				if (dbTempfile != null)
					dbTempfile.Close();
				if (dbfile != null)
					dbfile.Close();

				dbTempfile = null;
				dbfile = null;
			}
		}

		private string GenerateTablesAndColumn(Tables tables)
		{
			StringBuilder sb = new StringBuilder();

			// wirite each table
			foreach(Table tbl in (from t in tables where !t.Ignore select t))
			{
				sb.AppendFormat("\t[TableName(\"{0}\")]", tbl.Name);

				if (tbl.PK!=null && tbl.PK.IsAutoIncrement)
				{
					if (tbl.SequenceName==null)
					{
						sb.AppendFormat("\t[PrimaryKey(\"{0}\")]", tbl.PK.Name);
					}
					else
					{
						sb.AppendFormat("\t[PrimaryKey(\"{0}\", sequenceName=\"{1}\")]", 
						                tbl.PK.Name, tbl.SequenceName);
					}
				}

				if (tbl.PK!=null && !tbl.PK.IsAutoIncrement)
				{
					sb.AppendFormat("\t[PrimaryKey(\"{0}\", autoIncrement=false)]", 
						                tbl.PK.Name);
				}

				sb.AppendLine();
				sb.AppendLine("\t[ExplicitColumns]");
				sb.AppendFormat("\tpublic partial class {0} : <#=RepoName#>.Record<{0}>",
				                tbl.ClassName);
				sb.AppendLine("\t{");

				// write each column
				foreach(Column col in from c in tbl.Columns where !c.Ignore select c)
				{
					if (col.Name!=col.PropertyName)
					{
						sb.AppendFormat("\t\t[Column(\"{0}\")]", col.Name);
					}
					else
					{
						sb.AppendLine("\t\t[Column]");
					}

					string chknull = PetaPocoCore.CheckNullable(col);
					sb.AppendFormat("\t\tpublic {0} {1} {2}",
					                col.PropertyType, chknull, col.PropertyName);
					sb.AppendLine();
					sb.AppendLine("\t\t{");
					sb.AppendLine("\t\t\tget");
					sb.AppendLine("\t\t\t{");
					sb.AppendFormat("\t\t\t\treturn _{0};",
					                col.PropertyName);
					sb.AppendLine();
					sb.AppendLine("\t\t\t}");

					sb.AppendLine("\t\t\tset");
					sb.AppendLine("\t\t\t{");
					sb.AppendFormat("\t\t\t\t_{0} = value;",
					                col.PropertyName);
					sb.AppendLine();
					sb.AppendFormat("\t\t\t\tMarkColumnModified(\"{0}\");",
					                col.Name);
					sb.AppendLine();
					sb.AppendLine("\t\t\t}");
					sb.AppendLine("\t\t}");

					sb.AppendFormat("\t\t{0}{1} _{2};",
					                col.PropertyType, chknull, col.PropertyName);
					sb.AppendLine();
				}
				sb.AppendLine("\t}");
			}

			return sb.ToString();
		}
	}
}

