using System;
using System.Configuration;

namespace petapoco_gendbcode
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("==================================================");
			Console.WriteLine ("====================Start!========================");
			Console.WriteLine ("==================================================");

			string Namespace = ConfigurationManager.AppSettings["Namespace"];
			string RepoName = ConfigurationManager.AppSettings["RepoName"];
			string ConnectionStringName = ConfigurationManager.AppSettings["ConnectionStringName"];
			string ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
			string ProviderName=ConfigurationManager.ConnectionStrings[ConnectionStringName].ProviderName;

			// get all tables
			PetaPocoCore ppc = new PetaPocoCore(ConnectionString, ProviderName);
			var tables = ppc.LoadTables();

			if (string.IsNullOrEmpty(Namespace))
				Namespace = "PetaPoco";
			if (string.IsNullOrEmpty(RepoName))
				RepoName = "PetaPocoDB";

			// generate Database.cs
			if (tables.Count>0)
			{
				PetaPocoGenerator ppg = new PetaPocoGenerator();
				ppg.GenerateDB(Namespace, RepoName, ConnectionStringName, tables);
			}
			
			Console.WriteLine ("==================================================");
			Console.WriteLine ("====================Completed!====================");
			Console.WriteLine ("==================================================");
		}
	}
}
