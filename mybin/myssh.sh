#!/bin/bash

echo "Select the server you want to Connect: "
echo "*     1) 193.168.221.194(lrt)"
echo "*     2) 10.124.201.120(lrt)"
echo "*     3) 193.168.192.197(pt-tool RHEL4_x64)"
echo "*     4) 193.168.207.167(pt-tool RHEL5.4_x64)"
echo "*     5) 193.168.242.97 (pt-tool RHEL6.0_x64)"
echo "*     6) 193.168.221.166(lrt)"
echo "*     7) 192.168.122.206(localhost)"

echo -n "You select is: "
read choice

autologin_exp=/home/wangyubin/mybin
case $choice in
    "1")
        expect "$autologin_exp/autologin.exp" 193.168.221.194
        ;;
    "2")
        expect "$autologin_exp/autologin.exp" 10.124.201.120 root rootroot
        ;;
    "3")
        expect "$autologin_exp/autologin.exp" 193.168.192.197 root fnst123
        ;;
    "4")
        expect "$autologin_exp/autologin.exp" 193.168.207.167 root fnst123
        ;;
    "5")
        expect "$autologin_exp/autologin.exp" 193.168.242.97 root fnst123
        ;;
    "6")
        expect "$autologin_exp/autologin.exp" 193.168.221.166
        ;;
    "7")
        expect "$autologin_exp/autologin.exp" 192.168.122.206
        ;;
    "*")
        echo "You should input the right choice!"
        ;;
esac
