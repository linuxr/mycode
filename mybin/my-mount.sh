#!/bin/bash

echo "You can mount Drives below:"
echo "*) default mount All windows drives!"
echo "1) mount windows C"
echo "2) mount windows D"
echo "3) mount windows E"
echo "4) mount Fedora16-x86_64 iso"
echo "5) mount 10.167.129.4/it-resource"
echo "6) mount my vmxp's share dir"
echo -n "You select is: "
read choice
case $choice in
    "1")
        sudo mount -t ntfs /dev/sda1 /mnt/C
        ;;
    "2")
        sudo mount -t ntfs /dev/sda2 /mnt/D
        ;;
    "3")
        sudo mount -t ntfs /dev/sda3 /mnt/E
        ;;
    "4")
        sudo mount -o loop /mnt/D/F/tools/linux/Fedora-16-x86_64-DVD.iso /mnt/cdrom
        ;;
    "5")
        sudo mount -t cifs -o username=user,password=fnst //10.167.129.4/it-resource /mnt/smba
        ;;
    "6")
        sudo mount -t cifs -o username=wangyb,password="" //192.168.122.138/share /mnt/vm_share
        ;;
    "*")
        echo "please select a valid number!"
        ;;
esac

